<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Mariku.asia – Registration</title>
		<meta name="description" content="We are a Chinese, Bahasa &amp; English online &amp; mobile games portal for Southeast Asia. We provide content for players to enjoy without the hassle of restrictive paywalls and complicated computer requirements.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/main.css">
		
		<!-- javascripts -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<script src="js/vendor/jquery-1.11.0.min.js"></script>
		<script src="js/vendor/bootstrap.min.js"></script>
	</head>
	<body><!--body onLoad="onLoad()" class="playcube"-->
	<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!-- all components must be placed here -->
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="image-text-overlay">
						<img src="mariku.jpg" id="mariku" alt="Mariku Banner Options" class="img-responsive" />
						<h3>
							Register Now<br />
							Get Your Free 50 Mariku Game Points
						</h3>
					</div>
				</div>
				<div class="col-md-4 message-block">
					<div>
						<h3>You have successful sign up your Mariku Games account!</h3>
						<h3>Please check your email account for verification.</h3>
						<h3>Thank you.</h3>
					</div>
					<div>
						<h3>
							注册成功！
						</h3>
						<h3>
							感谢您注册魔力库游戏账号。
						</h3>
						<h3>
							请检查您的电子邮件进行验证。
						</h3>
						<h3>
							谢谢。
						</h3>
				</div>
			</div>
		</div>
	</body>

	<script type="text/javascript">
		$(function() {

			$("h3")
				.wrapInner("<span>")

			$("h3 br")
				.before("<span class='spacer'>");
		});
	</script>
</html>