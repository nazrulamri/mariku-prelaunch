<?php
function error_message($error_id){
	switch ($error_id) {
		case 1:
			return "Email address has been registered. Use another email address.<br />";
			break;
		case 2:
			return "Mobile number has been registered. Use another mobile number.<br />";
			break;
		case 3:
			return "The system could not verify your information. Kindly verify your information is correct, and try send in again.";
			break;
		case 4:
			return "Some of your entries are missing information. Fill them up again.<br />";
			break;
		default:
			return "";
	}
}?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Mariku.asia – Registration</title>
		<meta name="description" content="We are a Chinese, Bahasa &amp; English online &amp; mobile games portal for Southeast Asia. We provide content for players to enjoy without the hassle of restrictive paywalls and complicated computer requirements.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="css/main.css">
		
		<!-- javascripts -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<script src="js/vendor/jquery-1.11.0.min.js"></script>
		<script src="js/vendor/bootstrap.min.js"></script>
		<script src="js/vendor/bootstrapValidator.min.js"></script>
	</head>
	<body><!--body onLoad="onLoad()" class="playcube"-->
	<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!-- all components must be placed here -->
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="image-text-overlay">
						<img src="mariku.jpg" id="mariku" alt="Mariku Banner Options" class="img-responsive" />
						<h3>
							Register Now<br />
							Get Your Free 50 Mariku Game Points
						</h3>
					</div>
					<div id="error-messages">
						<h3><?php echo isset($_GET['err']) ? error_message($_GET['err']) : ''; ?></h3>
					</div>
				</div>
				<div class="col-md-4">
					<form id="register" class="form-horizontal panel center-block" role="form" action="process.php" method="post">
						<div class="form-group">
							<label for="email" class="col-md-4 control-label">Email<br />(电邮)</label>
							<div class="col-md-8">
								<input type="text" name="email" id="email" class="form-control" placeholder="Email (电邮)" />
							</div>
						</div>

						<div class="form-group">
							<label for="passwd1" class="col-md-4 control-label">Password<br />(密码)</label>
							<div class="col-md-8">
								<input type="password" name="passwd1" id="passwd1" class="form-control" placeholder="Password (密码)" />
							</div>
						</div>

						<div class="form-group">
							<label for="passwd2" class="col-md-4 control-label">Retype Password<br />(确定密码)</label>
							<div class="col-md-8">
								<input type="password" name="passwd2" id="passwd2" class="form-control" placeholder="Retype Pwd (确定密码)" />
							</div>
						</div>

						<div class="form-group">
							<label for="country-code" class="col-md-4 control-label">Country Code<br />(国家代码)</label>
							<div class="col-md-8">
								<select name="country-code" id="country-code" class="form-control">
									<option value="+60" selected>+60 (Malaysia)</option>
									<option value="+65">+65 (Singapore)</option>
									<option value="+62">+62 (Indonesia)</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label for="mobile" class="col-md-4 control-label">Mobile Number<br />(手机号码)</label>
							<div class="col-md-8">
								<input type="text" name="mobile" id="mobile" class="form-control" placeholder="Mobile (手机号码)" />
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn btn-primary col-md-offset-1 col-md-10" name="signup" id="signup"><h3>Sign Up!<br />报名</h3></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>

	<script type="text/javascript">
	$(function() {
		$('h3')
			.wrapInner('<span>');

		$('h3 br')
			.before('<span class="spacer">');

		$('#register').bootstrapValidator({
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			message: 'This value is not valid.',
			fields: {
				email: {
					validators: {
						notEmpty: {
							message: 'Supply an email address.'
						},
						emailAddress: {
							message: 'Not a valid email address.'
						}
					}
				},
				passwd1: {
					validators: {
						notEmpty: {
							message: 'Supply a password.'
						},
						stringLength: {
							min: 6,
							max: 24,
							message: "Password must be between 6 to 24 characters."
						},
						identical: {
							field: 'passwd2',
							message: 'Passwords must be the same.'
						}
					}
				},
				passwd2: {
					validators: {
						identical: {
							field: 'passwd1',
							message: 'Passwords must be the same.'
						}
					}
				},
				mobile: {
					trigger: 'keyup',
					validators: {
						notEmpty: {
							message: "Please enter your mobile number."
						},
						digits: {
							message: "Please enter numbers only."
						}
					}
				}
			}
		});
	});
	</script>
</html>