<?php
/* 
Taken from the following website:
http://culttt.com/2012/10/01/roll-your-own-pdo-php-class/
*/

//define connections — please edit this part only.
define("DB_HOST", "localhost");
define("DB_USER", "mariku_asia");
define("DB_PASS", "M4r1ku_4s14");
define("DB_NAME", "mariku_asia");
//End of user-defined limitation.


class DBConnection {
	private $host = DB_HOST;
	private $user = DB_USER;
	private $pass = DB_PASS;
	private $dbname = DB_NAME;

	private $dbh;
	private $error;

	private $stmt;

	public function __construct() {
		//Set DSN
		$dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

		//Set options.
		$options = array(
			PDO::ATTR_PERSISTENT => true,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		);

		//Create a new PDO instance.
		try {
			$this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
		}
		//Catch any errors
		catch (PDOException $e){
			$this->error = $e->getMessage();
		}
	}

	// setters and getters
	public function getErrors() {
		return $this->error;
	}

	// prepare a query statement first.
	public function query($query) {
		$this->stmt = $this->dbh->prepare($query);
	}

	// This is used to tie down variable names in query() above to variables
	// in local PHP scripts. E.g. bind(':name', $name);
	public function bind($param, $value, $type = null) {
		if (is_null($type)) {
			echo $type;
			switch (true) {
				case is_int($value): $type = PDO::PARAM_INT;break;
				case is_bool($value): $type = PDO::PARAM_BOOL; break;
				case is_null($value): $type = PDO::PARAM_NULL; break;
				default : $type = PDO::PARAM_STR;
			}
		}
		$this->stmt->bindValue($param, $value, $type);
	}

	public function execute() {
		return $this->stmt->execute();
	}

	public function resultset() {
		$this->stmt->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function single() {
		$this->stmt->execute();
		return $this->stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function rowCount() {
		$this->stmt->rowCount();
	}

	public function lastInsertId() {
		return $this->dbh->lastInsertId();
	}

	public function beginTransaction() {
		return $this->dbh->beginTransaction();
	}

	public function endTransaction() {
		return $this->dbh->commit();
	}

	public function cancelTransaction() {
		return $this->dbh->rollBack();
	}

	public function debugDumpParams() {
		return $this->stmt->debugDumpParams();
	}
}
?>