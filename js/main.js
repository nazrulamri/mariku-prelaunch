(function($) {
    $.fn.bootstrapValidator.validators.customPhone = {
        html5Attributes: {
            message: 'Your phone number isn\'t correct.',
            field: 'country-code',
            check_only_for: 'my,sg,id'
        },

        /**
         * Return true if the input value contains a valid US phone number only
         *
         * @param {BootstrapValidator} validator Validate plugin instance
         * @param {jQuery} $field Field element
         * @param {Object} options Consist of key:
         * - message: The invalid message
         * - field: The name of field that will be used to get selected country
         * - check_only_for : Coma separated ISO 3166 country codes // i.e. 'us,ca'
         * @returns {Boolean}
         */
        validate: function(validator, $field, options) {
            var value = $field.val();
            if (value == '') {
                return true;
            }

            var countryField = validator.getFieldElements(options.field);

            if (countryField == null) {
                return true;
            }

            var countryCode = countryField.val().trim();

            if(countryCode == ''){
                return true;
            }

            var check_list = options.check_only_for.toUpperCase().split(',')

            if(check_list.indexOf(countryCode.toUpperCase()) == -1) {
                return true;
            }

            value = value.replace(/\(|\)|\s+/g, '');
            return (/^(?:1\-?)?(\d{3})[\-\.]?(\d{3})[\-\.]?(\d{4})$/).test(value);
        }
    }
}(window.jQuery));