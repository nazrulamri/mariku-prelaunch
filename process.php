<?php
//define connections — please edit this part only.
define("DB_HOST", "localhost");
define("DB_USER", "mariku_asia");
define("DB_PASS", "M4r1ku_4s14");
define("DB_NAME", "mariku_asia");
//End of user-defined limitation.

// include the database class.
require_once './common/MysqliDb.php';

// include error-logging class.
require_once './common/KLogger.php';

// Set up an error message variable.
$error_msg = new KLogger('../log/app_log.txt', KLogger::DEBUG );

if (isset($_POST)) {
	$email = isset($_POST['email']) ? strip_tags($_POST['email']) : '';
	$pass = isset($_POST['passwd1']) && isset($_POST['passwd2']) ? crypt($_POST['passwd2']) : '';
	$prefix = isset($_POST['country-code']) ? $_POST['country-code'] : '';
	$mobile = isset($_POST['mobile']) ? $_POST['mobile'] : '';

	$error_msg->LogDebug('$email: '.$email);
	$error_msg->LogDebug('$pass: '.$pass);
	$error_msg->LogDebug('$prefix: '.$prefix);
	$error_msg->LogDebug('$mobile: '.$mobile);

	if (isset($email) && isset($pass) && isset($prefix) && isset($mobile)) {

		$error_msg->LogDebug('isset() is available.');
		// Set up DB Connection
		$db = new MysqliDb(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		// Set up an array of flags to check if there are existing emails and mobile numbers in the database.
		$flags = array ('email_exist' => false, 'mobile_exist' => false);

		// check if there are any existing emails in the database.
		$db->where('email', $email);
		$flags['email_exist'] = is_Exists($db->getOne('users', 'COUNT(email)'));

		// Check if there are any existing mobile numbers in the database.
		$db->where('mobile_no', $mobile);
		$flags['mobile_exist'] = is_Exists($db->getOne('users', 'COUNT(mobile_no)'));

		$error_msg->LogDebug('Email exists? ' . print_r($flags['email_exist']));
		$error_msg->LogDebug('Mobile exists? ' . print_r($flags['mobile_exist']));

		if ($flags['email_exist']) {
			header('location: ./?err=1');
			exit;
		}
		elseif ($flags['mobile_exist']) {
			header('location: /?err=2');
			exit;
		}
		else {
			$data = Array (
				"email" => $email,
				"password" => $pass,
				"country_prefix_no" => $prefix,
				"mobile_no" => $mobile,
				"is_validated" => 0,
				"wallet_points" => 50
				);

			$id = $db->insert('users', $data);

			if ($id) {
				header('location: result.php');
				exit;
			}
			else {
				header('location: ./?err=3');
				exit;
			}
			
		}
	}
	else {
		header('location: ./?err=4');
		exit;
	}
	
}

else {
	header('location: ./?err=3');
	exit;
}

/**
 * Function to check if there are existing emails or mobile number in the system.
 * The database query beforehand must be in the form "SELECT COUNT(<column_name>) FROM <table>..."
 * 
 * @param array $resultset		resultant set from database query
 * 
 * @return boolean	Boolean indicating whether the resultset has more than 0 count in the system.
 */
function is_Exists($resultset) {
	$how_many_records = 0;
	foreach ($resultset as $counter) { $how_many_records = $counter; }
	if ($how_many_records > 0) return true;
	else return false;
}
?>